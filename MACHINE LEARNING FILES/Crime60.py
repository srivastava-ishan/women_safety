import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
# get_ipython().run_line_magic('matplotlib', 'inline')


df = pd.read_csv("crime60.csv")



df.drop(['Date', 'crime place?', 'url',], axis =1, inplace=True)



data = df.values

np.random.seed(24)

np.random.shuffle(data)


split = int(0.85* data.shape[0])

train = data[:split]
test = data[split:]

X_train = train[:,:4]
y_train = train[:,4]

X_test = test[:,:4]
y_test = test[:,4]


a =data[:, :4]
b =data[:,4]



RF = RandomForestClassifier(100)




RF.fit(X_train, y_train)




print("Accuracy: ", RF.score(X_test, y_test)*100,"%")




Input = [1,4,3,2]




a=  RF.predict_proba([Input])




y=[1, 3, 5, 7]



plt.title('CRIME PREDICTION')
plt.xlabel('CRIME')
plt.ylabel('PROBABILTY OF CRIME') 
plt.bar(y, (a*100).tolist()[0],width=1, tick_label=['verbal molest','physical molest','Rape','Rape including Abduction'])
plt.legend(["Input: "+ str(Input)])
plt.show()



# joblib.dump(RF, 'crime.pkl')




