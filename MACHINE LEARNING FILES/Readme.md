
This Readme contains Machine Learning Implementation as well as API Development.         


Tools Used for Machine Learning Implementation include:
1.Numpy (Computational Purpose)
2.Pandas (Loading and Manipulating Dataset)
3.Matplotlib (Plotting and visualisation Purpose)
4.Sklearn (Implementing Machine Learning Algorithm)
5.Pickle (Used For Pickling Purposes)

Tools Used For API Implementation include:
1.Flask

Process involved in Implementation:

1. python Crime60.py

This Command will result in a creation of file named Crime.pkl for saving the model.

After the model is set, we will create an API endpoint which takes input params and yeild the probability of crime
that can happen due to the enviironment variables involved.

There are 4 inputs for API, they are:
-Gender Diversity
-Crowdness
-lightening 
-virtues

These inputs represent different environment variables with values representing the states which range from 1 to 4, where 1 being the least promoting and 4 being the most promoting to the crime.

The API takes input in the form(values are separated by '&') :

	http://url-address:5000/api/N1&N2&N3&N4
	
		where,
		 url-address -> ip-address of the host
		 N1 -> value for gender diversity
		 N2 -> value for the crowdness of the place
		 N3 -> value for the lightening condition of the place
		 N4 -> value for the virtueness
		 

2.python web.py
This command will start flask server which acts as API endpoint.

Note: 
For hosting on Server, make the host in the file to be 0.0.0.0





   


