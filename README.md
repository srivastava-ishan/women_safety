Approach:
In order to solve this problem, the team searches out a solution after which every region in India will be classified and marked with the crime- type and the percentage probability 
of a particular crime in that region.The crime on the women are classified into two sets of crimes, classified on the basis of it's, direct dependence on the environment variables 
(whose presence or absence can directly alter the outcome of the event).These six environmental variables are, 1.Sex ratio, 2.Lighting (Illumination) 3.Number of people around, 4.Virtue Quotient, 
5. Surveillance and 6.Escaping possibility.The first set of crime, that is dependent on these environment variables, include rape, eve-teasing, molestation, kidnapping etc.The chances of these crimes 
exceed whenever the factors supporting them are available and vice versa.The second set of the problem includes child marriage, dowry, foeticide etc.It is seen that these set of problems have a negligible
co-relation with these environment variables.In the first module, the first set of crimes will be examined and these crimes(rape, eve-teasing, molestation, kidnapping etc.) will be further classified according 
to the relative severity of the crime ,and arranged into the pyramid of severity whose top layer is least severe and the bottom layer is the most severe.

Work-flow:
To make necessary dataset, data about various PAST crimes will be scraped from the different news sources and each of these past news will be broken down into the crime type and the environment variable(discussed earlier).
After that, machine learning model will be trained from the prepared dataset containing the crime type and factors involved in the crime so that model can further predict crime based on inputs of an environment variable.The
web and mobile interfaces will be prepared , from which user enters the details of his surrounding.This input of the area will act as an input to the model and the probability of all types of crime on the basis of entered input 
will be generated for that region and will be plotted on the map. The plotting of the map would be dynamic and changes as the accuracy of model increases and on the change in various environment variables.In the second module, the 
second set of crimes(child marriage, dowry, foeticide)will be examined, and since these are independent of the environment variables, user does not need to enter any information for them.Regions will be classified for the ,second 
set of crimes, according to the PAST cases registered with government.
Prediction of crime type and its probability related to women ,in different regions of India using Machine Learning models

After implementation.
1.) Any women can find out which region is safer and which is not
2.) It helps in selecting the safest possible route to reach a destination amongst several routes